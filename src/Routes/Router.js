import {lazy }from "react"
import Layout from "../components/Common/Layout"
import Home from "../pages/home/Home"
import List from "../pages/list/List"
import Report from "../pages/Report/Report"
import Setting from "../pages/Setting/Setting"
import Payroll from "../pages/payroll/Payroll"
import Leaves from "../pages/Leaves/Leaves"
import Attendance from "../pages/Attendance/Attendance"

// Main Routes:

const DashboardLayout = Layout(Home)
const EmployeeLayout = Layout(List)
const AttendanceLayout =Layout(Attendance)
const LeaveLayout = Layout(Leaves)
const PayrollLayout = Layout(Payroll)
const SettingLayout =Layout(Setting) 
const ReportLayout = Layout(Report)


export const empRoutes = [{
    path: "/",
    component: <DashboardLayout />,
    name: "Dashboard"
}, {
    path: "/Employee",
    component: <EmployeeLayout />,
    name: "Employee"
}, {
    path: "/Attendance",
    component: <AttendanceLayout />,
    name: "Attendance"
}, {
    path: "/Leave",
    component: <LeaveLayout />,
    name: "Leave"
}, {
    path: "/Payroll",
    component:<PayrollLayout />,
    name: "Payroll"
}, {
    path: "/Setting",
    component:<SettingLayout />,
    name: "Setting"
}, {
    path: "/Report",
    component:<ReportLayout />,
    name: "Report"
},{
    path:"/DashboardLayout",
    component: <DashboardLayout/>,
    name:"DashboardLayout"
}]


