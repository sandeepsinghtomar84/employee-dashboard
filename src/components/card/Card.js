import { WidthFull } from '@mui/icons-material';
import { Container, Typography } from '@mui/material'
import Box from '@mui/material/Box';
import React from 'react';
import LinearProgress, { linearProgressClasses } from '@mui/material/LinearProgress';


const Card = (props) => {
    const {title,give,balance}= props; 
    
  return (
    <Container maxWidth="sm" sx={{border:".5px solid black",borderRadius:"5px",backgroundColor:"white",width:"24%",margin:"6px"}}>
        <Box  sx={{display:"flex",justifyContent:"space-between",}}>
        <Typography variant='p' sx={{fontSize:"12px",marginTop:"3%"}}>{title}</Typography>
        <Typography variant='p' sx={{fontSize:"12px",marginTop:"3%"}}>Granted:{give}</Typography>
        </Box>

        <Box sx={{display:"flex",flexDirection:"column",alignItems:"center",padding:"27%"}}>
        
            <Typography  variant='p' WidthFull sx={{fontSize:"10px"} }>{balance}</Typography>
            
            
            <Typography variant='p' WidthFull sx={{fontSize:"10px",}}>Balance</Typography>
            <Typography variant='h6' WidthFull sx={{color:"blue",fontSize:"12px"}}>View Details</Typography>
            
            {/* <BorderLinearProgress variant="determinate" value={20} /> */}

        </Box>
        
    </Container>
  )
}

export default Card;