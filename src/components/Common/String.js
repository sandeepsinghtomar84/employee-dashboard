import DashboardIcon from "@mui/icons-material/Dashboard";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import SettingsApplicationsIcon from "@mui/icons-material/SettingsApplications";
import ExitToAppIcon from "@mui/icons-material/ExitToApp";
import AccessAlarmIcon from '@mui/icons-material/AccessAlarm';
import AssessmentIcon from '@mui/icons-material/Assessment';
import EnergySavingsLeafIcon from '@mui/icons-material/EnergySavingsLeaf';
import CurrencyRupeeIcon from '@mui/icons-material/CurrencyRupee';
export const SideBarList = [{
    path: "/",
    icon: <DashboardIcon />,
    name: "Dashboard"
}, {
    path: "/Employee",
    icon: <PersonOutlineIcon />,
    name: "Employee"
}, {
    path: "/Attendance",
    icon: <AccessAlarmIcon />,
    name: "Attendance"
}, {
    path: "/Leave",
    icon: <EnergySavingsLeafIcon />,
    name: "Leave"
}, {
    path: "/Payroll",
    icon:<CurrencyRupeeIcon />,
    name: "Payroll"
}, {
    path: "/Setting",
    icon:<SettingsApplicationsIcon />,
    name: "Setting"
}, {
    path: "/Report",
    icon:<AssessmentIcon />,
    name: "Report"
}, {
    path:"",
    icon:<ExitToAppIcon />,
    name:"logout"
}]