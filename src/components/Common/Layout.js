import React from "react"
import Sidebar from "../sidebar/Sidebar"
import Navbar from "../navbar/Navbar";
import "./layout.css"

const Layout = (Component) => (props) => (
    <>
        <div className="main">
            
            <Sidebar />
            <div className="mainContainer">
                <Navbar />
                <Component {...props} />
            </div>
        </div>

    </>
)

export default Layout;