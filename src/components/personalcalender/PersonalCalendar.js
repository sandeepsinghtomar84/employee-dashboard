import React from 'react';
import dayjs from 'dayjs';
import { DemoContainer, DemoItem } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { StaticDatePicker } from '@mui/x-date-pickers/StaticDatePicker';
import "./style.scss"
import DashboardLayout from '../DashboardLayot/DashboardLayout';

const PersonalCalendar = () => {
    const currentDate = dayjs(); // Get the current date using dayjs

    return (
        <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DemoContainer
                components={['StaticDatePicker']}
            >
                <DemoItem label="Personal calender">
                    <DashboardLayout/>

                    {/* Pass the current date as the defaultValue */}
                    <StaticDatePicker className='new-size' defaultValue={currentDate} />
                </DemoItem>
            </DemoContainer>
        </LocalizationProvider>
    );
};

export default PersonalCalendar;
