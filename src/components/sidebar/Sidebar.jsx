import "./sidebar.scss";
import { Link } from "react-router-dom";
import devLogo from "../../assets/logo.png"
import { Avatar } from "@mui/material";
import { SideBarList } from "../Common/String"
const Sidebar = () => {
  return (
    <div className="sidebar">
      <div className="top">
        <Link to="/" className="linkstyle" >
          <Avatar src={devLogo} style={{ border: "none", display: "unset" }} />
        </Link>
      </div>
      <hr />
      <div className="center">
        <ul>
          {SideBarList?.map((elm) => {
            return(
              <Link to={elm.path} className="linkstyle">
            <li>
                <p className="icon">{elm.icon}</p>
                <span>{elm.name}</span>
            </li>
              </Link>
            )
          })}
        </ul>
    </div>
      </div>
  );
};

export default Sidebar;
