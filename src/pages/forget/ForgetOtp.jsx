import react from "react";
import { Container } from "@mui/material";
import Box from "@mui/material/Box";
import logo from "../../assets/logo.png";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import Button from '@mui/material/Button';


function ForgetOtp() {
    return (
        <>
            <Container maxWidth="sm" sx={{  marginTop: "10%" }}>
                <Box>
                    <Box>
                        <img src={logo} sx={{ width: 5, height: 18, }} />
                    </Box>
                    <Typography variant="h4" sx={{ display: "flex", justifyContent: "center", }}>
                        Varification

                    </Typography>
                    <Typography variant="h6" sx={{ display: "flex", justifyContent: "center", marginTop: "9%" }}>Enter Your Verification Code </Typography>
                    <Box sx={{display:'flex',}} gap={5}>
                        <TextField
                            margin="normal"
                            required
                            maxWidth="xl" name="first" autoFocus />
                            <TextField
                            margin="normal"
                            required
                             name="second" autoFocus/>
                            <TextField
                            margin="normal"
                            required
                             name="third" autoFocus/>
                            <TextField
                            margin="normal"
                            required
                             name="fourth" autoFocus/>

                    </Box>

                </Box>
                <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2, }}
                        >
                            Submit 
                        </Button>


            </Container>

        </>
    )
};

export default ForgetOtp;
