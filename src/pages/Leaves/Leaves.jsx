import React from 'react'
import Card from '../../components/card/Card'
import { Box } from '@mui/material'

const Leaves = () => {
  return (
    <Box sx={{display:"flex",flexWrap:"wrap"}}>
        <Card title={"Loss Of Pay"} give={0} balance={0}/> 
        <Card title={"Comp-Off"} give={1} balance={4}/> 
        <Card title={"Casual Leaves"} give={4} balance={9}/> 
        <Card title={"Earned Leaves"} give={9} balance={4}/> 
        <Card title={"Paternity Leaves"} give ={8} balance={5}/> 
        <Card title={"Work From Home"} give={5} balance={3}/> 
        <Card title={"Marriage Leaves"} give={4} balance={2}/> 
        <Card title={"Birthday Leave"} give={2} balance={0}/> 
        <Card title={"Casuality Leave"} give={6} balance={0}/> 
        </Box>
  )
}

export default Leaves;