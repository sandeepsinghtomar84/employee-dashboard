import Home from "./pages/home/Home";
import Login from "./pages/login/Login";
import List from "./pages/list/List";
import Single from "./pages/single/Single";
import New from "./pages/new/New";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { productInputs, userInputs } from "./formSource";
import "./style/dark.scss";
import { useContext } from "react";
import { DarkModeContext } from "./context/darkModeContext";
import Forget from "./pages/forget/Forget";
import  ForgetPassword  from "./pages/forget/ForgetPassword";
import ForgetOtp from "./pages/forget/ForgetOtp";
import  {empRoutes} from "./Routes/Router";
import Card from "./components/card/Card";

function App() {
  const { darkMode } = useContext(DarkModeContext);
console.log("empRoutes",empRoutes)
  return (
    <div className={darkMode ? "app dark" : "app"}>
      
        <Routes>
        {empRoutes?.map((elm)=>{
          return(
            <Route path={elm.path} element={elm.component} />
          )
        })}
        
            <Route path="login" element={<Login />} />
            <Route path="forget" element={<Forget />} />
            <Route path="forgetpassword" element={<ForgetPassword />} />
            <Route path="forgetotp" element={<ForgetOtp/>} />
            {/* <Route  path="card" element={<Card/>}></Route> */}
        </Routes>
       
        
    </div>
  );
}

export default App;
