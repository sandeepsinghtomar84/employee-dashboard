import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { DarkModeContextProvider } from "./context/darkModeContext";
import { BrowserRouter as Router } from "react-router-dom";

ReactDOM.render(
  <React.StrictMode>
    <DarkModeContextProvider>
    <Router >
      <App />
    </Router>
    </DarkModeContextProvider>
  </React.StrictMode>,
  document.getElementById("root")
);





{/* <Typography variant='h3' sx={{ display: "flex", textAlign: "center", justifyContent: "center", margin: "3%", }}>Enter Your OTP Text</Typography>
                <Typography variant='subtitle' sx={{ display: "flex", textAlign: "center", justifyContent: "center", }}>We just send a message,please open it and enter the OTP code in that message below to identify your account.</Typography>
                <Box> <TextField margin="normal" required autoFocus sx={{ display: "inline-block", }} /></Box>
                <Box sx={{ display: "flex", justifyContent: "center", }}>  <TextField margin="normal" required autoFocus sx={{ display: "inline-block", }} /></Box>
                <Box> <TextField margin="normal" required autoFocus /></Box>
                <Box>  <TextField margin="normal" required autoFocus /></Box>
                <Box>  <TextField margin="normal" required autoFocus /></Box>
                <Box>  <TextField margin="normal" required autoFocus /></Box>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2, width: "30%", display: 'flex', alignContent: "center" }}
                >
                    Submit OTP
                </Button> */}